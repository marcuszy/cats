# Cats

Cats! This project is only for fun and to try some things out. As a plus one gets to see some nice cat images. So you basically never become frustrated while developing because you always have the ability to watch some nice cats.

Clone, install use for whatever you want.

## Feature requests

Feel free to contact me for feature requests.

## Demo

Demo of the project can be found [here](http://maszy.de)

## Install

`git clone https://gitlab.com/marcuszy/cats.git`

`npm install`

`ng serve`

## Credits

API for cat images was found here: [https://github.com/public-apis/public-apis](https://github.com/public-apis/public-apis)

The consumed api is this one: `https://aws.random.cat/meow`
