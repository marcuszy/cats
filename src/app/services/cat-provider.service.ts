import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ICat } from '../interfaces/ICat';

@Injectable({
  providedIn: 'root',
})
export class CatProviderService {
  private catsURL = 'https://aws.random.cat/meow';

  constructor(private http: HttpClient) {}

  /**
   * Loads a single cat image.
   *
   * @returns {Promise<ICat>}
   * @memberof CatService
   */
  retrieveCatImage() {
    return this.http.get<ICat>(this.catsURL).toPromise();
  }
}
