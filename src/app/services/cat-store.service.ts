import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ICat } from '../interfaces/ICat';
import { CatProviderService } from './cat-provider.service';

@Injectable({
  providedIn: 'root',
})
export class CatStoreService {
  get catImages$(): Observable<ICat> {
    return this.cats$.asObservable();
  }

  private cats$: Subject<ICat> = new Subject<ICat>();

  constructor(private cats: CatProviderService) {}

  async loadNextCatImage() {
    try {
      let singeCatImage = await this.cats.retrieveCatImage();
      this.cats$.next(singeCatImage);
    } catch (er) {
      console.error('Image could not be loaded: ', er);
    }
  }
}
