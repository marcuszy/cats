import { TestBed } from '@angular/core/testing';

import { CatStoreService } from './cat-store.service';

describe('CatStoreService', () => {
  let service: CatStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
