import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisplayPictureComponent } from './display-picture/display-picture.component';
import { ImprintComponent } from './imprint/imprint.component';

@NgModule({
  declarations: [AppComponent, DisplayPictureComponent, ImprintComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
