import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisplayPictureComponent } from './display-picture/display-picture.component';
import { ImprintComponent } from './imprint/imprint.component';

const routes: Routes = [
  { path: 'cats', component: DisplayPictureComponent },
  { path: 'imprint', component: ImprintComponent },
  { path: '**', component: DisplayPictureComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
