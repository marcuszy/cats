import { Icu } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { ICat } from '../interfaces/ICat';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CatStoreService } from '../services/cat-store.service';

@Component({
  selector: 'app-display-picture',
  templateUrl: './display-picture.component.html',
  styleUrls: ['./display-picture.component.scss'],
})
export class DisplayPictureComponent implements OnInit {
  loading = true;
  catImage$: Observable<ICat>;
  constructor(private catStore: CatStoreService) {
    this.catImage$ = this.catStore.catImages$;
  }

  ngOnInit(): void {
    this.nextCat();
  }

  async nextCat() {
    this.loading = true;
    await this.catStore.loadNextCatImage().then(() => {
      this.loading = false;
    });
  }
}
